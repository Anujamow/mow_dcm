<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('client_model','client');
	}

	public function index()
	{
 		$this->load->helper('url');
		$this->load->view('client_view');
		
	}
	function customer()
	{
 		$this->load->helper('url');
		$this->load->view('customer_view');
		
	}
	function expiring_domains()
	{
 		$this->load->helper('url');
		$this->load->view('expiring_domains');
		
	}
function apiCall()
{
	$method=$this->input->get('method');
	$url=$this->input->get('url');
	$data=$this->input->get('data');
	
	$curl = curl_init();
	
	switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    //curl_setopt($curl, CURLOPT_USERPWD, "username:password");
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		'Access-Control-Allow-Origin: *'
	));
    
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_TIMEOUT, 60);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($curl);

    curl_close($curl);
    echo $result;
   // return $result;
}
	public function ajax_add()
	{
		$this->_validate();
		$data = array(
				'fullname' => $this->input->post('fullname'),
				'customerid' => $this->input->post('customerid'),
				'email' => $this->input->post('email'),
				'company' => $this->input->post('company'),
				//'dob' => $this->input->post('dob'),
			);
		$insert = $this->client->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
				'fullname' => $this->input->post('fullname'),
				'customerid' => $this->input->post('customerid'),
				'email' => $this->input->post('email'),
				'company' => $this->input->post('company'),
				//'dob' => $this->input->post('dob'),
			);
		$this->client->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}
	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		/* if($this->input->post('fullname') == '')
		{
			$data['inputerror'][] = 'fullname';
			$data['error_string'][] = 'full name is required';
			$data['status'] = FALSE;
		} */

		if($this->input->post('customerid') == '')
		{
			$data['inputerror'][] = 'customerid';
			$data['error_string'][] = 'Last name is required';
			$data['status'] = FALSE;
		}


		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Please enter email';
			$data['status'] = FALSE;
		}

		if($this->input->post('company') == '')
		{
			$data['inputerror'][] = 'company';
			$data['error_string'][] = 'company is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
