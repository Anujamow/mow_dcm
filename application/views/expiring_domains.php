<!DOCTYPE html>
<style>
#table_length
{
	margin-left: 248px !important;
}
#table
{
	margin-left: 203px;
}#table_info
{
	margin-left: 466px !important;
}
</style>
<html>
	<?php 
				
					include_once("sidebar_view.php");  
				
			
			?>
    <head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<link rel="stylesheet" href="<?php echo 'http://localhost/explore/'."assets/"; ?>bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo 'http://localhost/explore/'."assets/"; ?>datatables/css/dataTables.bootstrap.css">
	<link rel="stylesheet" href="<?php echo 'http://localhost/explore/'."assets/"; ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="<?php echo 'http://localhost/explore/'."assets/"; ?>css/style.css">
    </head> 
<body>
    <div class="container">
        <br />
       <!--<button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Add Client</button>-->
        
        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="90%">
            
        </table>
    </div>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>jquery/jquery-2.1.4.min.js"></script>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>

<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>datatables/js/dataTables.bootstrap.js"></script>

</body>
<script type="text/javascript">

var save_method;
var table;

$(document).ready(function() {

    check_expiring();
   

});


var data_table_obj=null;
function check_expiring()
{
	var reseller_id="445720";
	var url="https://httpapi.com/api/domains/search.json?auth-userid=445720&api-key=pdLCHrRUI7VWvDq2WPvVStngjMsMBpAJ&no-of-records=100&page-no=1&reseller-id"+reseller_id;
	//var url="https://test.httpapi.com/api/domains/details.json?auth-userid=445720&api-key=pdLCHrRUI7VWvDq2WPvVStngjMsMBpAJ&order-id=92773142&options=OrderDetails ";
	
	$.ajax({
		type:'GET',
		crossDomain: true,
		url : "<?php echo base_url('client/apiCall/')?>",
		datatype:'json',
		data:{
			"url":url,
			"data":{},
			"method":"GET"
		},
		success:function(data)
		{
			
			var data=JSON.parse(data);
			var rowdata = [];
			$.each(data,function(key,value)
			{
				
				
				var domain = value['entity.description'];
				var customer_id = value['entity.customerid'];
				var status= value['entity.currentstatus'];
				var order_id=value['orders.orderid'];
				var unixdate=value['orders.creationtime'];
				var date_creation = new Date(unixdate*1000);
				var cd=date_creation.toGMTString();
				var d = new Date(cd);
				var creation_date = $.datepicker.formatDate('dd-mm-yy', d);
				//alert(creation_date);
				
				
				var date = new Date();
				date.setDate(date.getDate() + 30);
				var futuredate = date.toISOString().split('T')[0];
				//alert(futuredate);
					
				var date1 = new Date();
				date1.setDate(date1.getDate());
				var currentdate = date1.toISOString().split('T')[0];
				//alert(currentdate);

				var unixenddate=value['orders.endtime'];
				var date_end = new Date(unixenddate*1000);
				var ed=date_end.toGMTString();
				var de = new Date(ed);
				var expiry_date = $.datepicker.formatDate('yy-mm-dd', de);
				var expiry_date1 = $.datepicker.formatDate('dd-mm-yy', de);
				//alert(expiry_date);
				if(expiry_date>=currentdate && expiry_date<=futuredate)
				{
					//alert();
					if(key=='recsindb' || key=='recsonpage')
					{ 
					}
					else
					{
						var actions='<a class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="renew_domain('+order_id+','+unixenddate+')"><i class="fa fa-refresh"></i></a>';
						var arry_data=[actions,domain,creation_date,expiry_date1];
						rowdata.push(arry_data);
					}
				}
				else
				{
				}
			})
			var columns = [];
							columns = 	[	
											{ title: "Actions" },
											{ title: "Domain Name" },
											{ title: "Registriation Date" },
											{ title: "Expiry Date" },
											
											
										];
									
							data_table_obj = $("#table").DataTable({columns: columns,data: rowdata });
    				
    			 }
	
		});
	
}

function renew_domain(id,date)
{
    var order_id=id;
	var exp_date=date;
	
	//var url="https://httpapi.com/api/domains/renew.json?auth-userid=445720&api-key=pdLCHrRUI7VWvDq2WPvVStngjMsMBpAJ&order-id="+order_id+"&years=1&exp-date="+exp_date+"&invoice-option=NoInvoice&discount-amount=0.0";
	$.ajax(
	{
		type:'GET',
		crossDomain: true,
		url : "<?php echo base_url('client/apiCall/')?>",
		datatype:'json',
		data:{
			"url":url,
			"data":{},
			"method":"GET"
		},
		success:function(data)
		{
			var data=JSON.parse(data);
			var cust_id=data.customerid;
			var cust_name=data.name;
			var cust_comp=data.company;
			var cust_email=data.useremail;
			$('#customerid').val(cust_id);
			$('#fullname').val(cust_name);
			$('#company').val(cust_comp);
			$('#email').val(cust_email);
			$('#customerid').val(cust_id);
			$('#modal_form').modal('show');
            $('.modal-title').text('Customer details');
			
		}
	});
            

}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo base_url('client/ajax_add')?>";
    } else {
        url = "<?php echo base_url('client/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
               
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

</script>
</html>