<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo 'http://localhost/explore/'."assets/"; ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo 'http://localhost/explore/'."assets/"; ?>css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				
					<span class="login100-form-title p-b-48">
						<a><img src="<?php echo 'http://localhost/explore/'."assets/"; ?>images/company_logo.png" alt="Logo" style="margin-top: -49px;height: 118px;" ></a>
					</span><div class="login-space">
					<form action="<?php echo base_url() ?>login/login" method="post"> 
					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
						
						<input type="text" name="email" class="input100" placeholder="Enter your username">
						
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input type="password" name="password" class="input100" placeholder="Enter your password">
						
						<span class="focus-input100"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Login
							</button>
						</div>
					</div>

					<div class="text-center p-t-115">
						<span class="txt1">
							Don’t have an account?
						</span>

						<a class="txt2" href="#">
							Sign Up
						</a>
					</div>
				</form>

			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>js/main1.js"></script>

</body>
</html>