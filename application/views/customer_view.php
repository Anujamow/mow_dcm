<!DOCTYPE html>
<style>
#table_length
{
	margin-left: 248px !important;
}
#table
{
	width="82%";
	 margin-left: 185px;
}
</style>
<html>
	<?php 
				
					include_once("sidebar_view.php");  
				
			
			?>
    <head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<link rel="stylesheet" href="<?php echo 'http://localhost/explore/'."assets/"; ?>bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo 'http://localhost/explore/'."assets/"; ?>datatables/css/dataTables.bootstrap.css">
	<link rel="stylesheet" href="<?php echo 'http://localhost/explore/'."assets/"; ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="<?php echo 'http://localhost/explore/'."assets/"; ?>css/style.css">
    </head> 
<body>
    <div class="container">
        <br />
       <!--<button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Add Client</button>-->
        
        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0">
		<tr>
		<th>Actions</th>
		<th>Customer Name</th>
		<th>Mobile No</th>
		<th>Email</th>
		</tr>
            
        </table>
    </div>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>jquery/jquery-2.1.4.min.js"></script>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo 'http://localhost/explore/'."assets/"; ?>datatables/js/dataTables.bootstrap.js"></script>

<div
<!-- Bootstrap modal -->
			
<div class="modal fade" id="modal_form" role="dialog">
	
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<h3 class="modal-title">Customer Details</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
			
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Full Name</label>
                            <div class="col-md-9">
                                <input id="fullname" name="fullname" placeholder="Full Name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Customer Id</label>
                            <div class="col-md-9">
                                <input id="customerid" name="customerid" placeholder="Customer id" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input id="email" name="email" placeholder="email id" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Company</label>
                            <div class="col-md-9">
                                <input id="company" name="company" placeholder="Company" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                      
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
</body>
<script type="text/javascript">

var save_method;
var table;

$(document).ready(function() {

    check_details();
   

});

function add_person()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Add Person');
}
var data_table_obj=null;
function check_details()
{
	var reseller_id="445720";
	var url="https://httpapi.com/api/domains/search.json?auth-userid=445720&api-key=pdLCHrRUI7VWvDq2WPvVStngjMsMBpAJ&no-of-records=100&page-no=1&reseller-id"+reseller_id;
	
	$.ajax({
		type:'GET',
		crossDomain: true,
		url : "<?php echo base_url('client/apiCall/')?>",
		datatype:'json',
		data:{
			"url":url,
			"data":{},
			"method":"GET"
		},
		success:function(data)
		{
			
			var data=JSON.parse(data);
			$.each(data,function(key,value)
			{
				var customer_id = value['entity.customerid'];
				if(key=='recsindb' || key=='recsonpage')
				{ 
				
				}
				else
				{
					get_cust(customer_id);
					
				}
			});		
    	}
	
	});
}
function get_cust(customer_id)
{
	var customer_id=customer_id;
	var url="https://httpapi.com/api/customers/details-by-id.json?auth-userid=445720&api-key=pdLCHrRUI7VWvDq2WPvVStngjMsMBpAJ&customer-id="+customer_id;
	$.ajax(
	{
		type:'GET',
		crossDomain: true,
		url : "<?php echo base_url('client/apiCall/')?>",
		datatype:'json',
		data:{
			"url":url,
			"data":{},
			"method":"GET"
		},
		success:function(data)
		{
			
			var data=JSON.parse(data);
			var rowdata = [];
			var cust_id=data.customerid;
			var cust_name=data.name;
			var cust_mob=data.telno;
			var cust_email=data.useremail;
			var actions='<a class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="view_customer('+cust_id+')"><i class="fa fa-eye"></i></a>';
			var table_cust_data='<tr><td>'+actions+'</td><td>'+cust_name+'</td><td>'+cust_mob+'</td><td>'+cust_email+'</td></tr>';
			
			$('#table').append(table_cust_data);
				//alert(cust_id);
				
					/* var actions='<a class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="edit_person('+customer_id+')"></i>Get Details</a>';
					var arry_data=[actions,cust_id];
					rowdata.push(arry_data);
			var columns = [];
							columns = 	[	
											{ title: "Actions" },
											{ title: "Customer Id" },
											
											
										];
									
							data_table_obj = $("#table").DataTable({columns: columns,data: rowdata }); */
							
    				
			
			
		}
	});
			

}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo base_url('client/ajax_add')?>";
    } else {
        url = "<?php echo base_url('client/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
               
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

</script>
</html>